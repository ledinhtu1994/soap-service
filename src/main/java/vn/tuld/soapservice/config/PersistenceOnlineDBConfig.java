package vn.tuld.soapservice.config;

import com.zaxxer.hikari.HikariDataSource;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * PersistenceOnlineDBConfig.
 *
 * @author namdx.
 * @version 1.0.0.
 * @created 25/Apr/2022 - 10:10 AM.
 */
@EnableJpaRepositories(
    basePackages = "vn.tuld.soapservice.repository",
    entityManagerFactoryRef = "onlineEntityManagerFactory",
    transactionManagerRef = "onlineTransactionManager"
)
@EnableTransactionManagement
@Configuration
public class PersistenceOnlineDBConfig {

  @Bean
  public DataSource onlineDataSource() {
//    log.info("start init HikariDataSource");
    HikariDataSource hikariDataSource = new HikariDataSource();
    hikariDataSource.setUsername("MBEIBOMNI");
    hikariDataSource.setPassword("MBEIBOMNI");
    hikariDataSource.setDriverClassName("oracle.jdbc.OracleDriver");
    hikariDataSource.setJdbcUrl("jdbc:oracle:thin:@10.22.19.128:1521/DVNHT1");
    hikariDataSource.setMaxLifetime(900000);
    hikariDataSource.setLeakDetectionThreshold(120000);
    hikariDataSource.setConnectionTimeout(30000);
    hikariDataSource.setPoolName("pool-name-hibernate-lab");
    hikariDataSource.setMinimumIdle(5);
    hikariDataSource.setMaximumPoolSize(90);
    hikariDataSource.setIdleTimeout(10);

//    log.info("init HikariDataSource done");
    return hikariDataSource;
  }

  @Bean(name = "onlineEntityManagerFactory")
  @Primary
  public LocalContainerEntityManagerFactoryBean onlineEntityManagerFactory() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(onlineDataSource());
    em.setPackagesToScan("vn.tuld");
    em.setPersistenceUnitName("online");
    em.setJpaVendorAdapter(vendorAdaptor());
    em.setPersistenceUnitName("onlinePersistenceContext");
    return em;
  }

  @Primary
  @Bean(name = "onlineTransactionManager")
  public PlatformTransactionManager onlineTransactionManager()
      throws InvalidAlgorithmParameterException, NoSuchPaddingException,
      IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException,
      InvalidKeyException {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(onlineEntityManagerFactory().getObject());
    return transactionManager;
  }

  @Bean(name = "onlineEntityManager")
  @Primary
  public EntityManager onlineEntityManager(
      @Qualifier("onlineEntityManagerFactory") EntityManagerFactory factory) {
    return factory.createEntityManager();
  }

  private HibernateJpaVendorAdapter vendorAdaptor() {
    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    vendorAdapter.setShowSql(true);
    return vendorAdapter;
  }
}
