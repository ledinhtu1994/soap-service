package vn.tuld.soapservice.endpoint;


import com.javatechie.spring.soap.api.employee.AddEmployeeRequest;
import com.javatechie.spring.soap.api.employee.AddEmployeeResponse;
import com.javatechie.spring.soap.api.employee.DeleteEmployeeRequest;
import com.javatechie.spring.soap.api.employee.DeleteEmployeeResponse;
import com.javatechie.spring.soap.api.employee.EmployeeInfo;
import com.javatechie.spring.soap.api.employee.GetEmployeeByIdRequest;
import com.javatechie.spring.soap.api.employee.GetEmployeeResponse;
import com.javatechie.spring.soap.api.employee.ServiceStatus;
import com.javatechie.spring.soap.api.employee.UpdateEmployeeRequest;
import com.javatechie.spring.soap.api.employee.UpdateEmployeeResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import vn.tuld.soapservice.model.Employee;
import vn.tuld.soapservice.services.EmployeeService;

@Endpoint
public class EmployeeEndPoint {
  private static final String NameSpace_URI = "http://www.javatechie.com/spring/soap/api/employee";

  @Autowired
  public EmployeeService employeeService;

  @PayloadRoot(namespace = NameSpace_URI, localPart = "addEmployeeRequest")
  @ResponsePayload

  public AddEmployeeResponse addEmployee(@RequestPayload AddEmployeeRequest request) {
    AddEmployeeResponse response = new AddEmployeeResponse();
    ServiceStatus serviceStatus = new ServiceStatus();

    Employee employee = new Employee();
    BeanUtils.copyProperties(request.getEmployeeInfo(), employee);
    employeeService.addEmployee(employee);
    serviceStatus.setStatus("SUCCESS");
    serviceStatus.setMessage("Content Added Successfully");
    response.setServiceStatus(serviceStatus);
    return response;
  }


  @PayloadRoot(namespace = NameSpace_URI, localPart = "getEmployeeByIdRequest")
  @ResponsePayload
  public GetEmployeeResponse getEmployee(@RequestPayload GetEmployeeByIdRequest request) {
    GetEmployeeResponse response = new GetEmployeeResponse();
    EmployeeInfo employeeInfo = new EmployeeInfo();
    BeanUtils.copyProperties(employeeService.getEmployeeById(request.getEmployeeId()),
        employeeInfo);
    response.setEmployeeInfo(employeeInfo);
    return response;

  }


  @PayloadRoot(namespace = NameSpace_URI, localPart = "updateEmployeeRequest")
  @ResponsePayload
  public UpdateEmployeeResponse updateEmployee(@RequestPayload UpdateEmployeeRequest request) {
    Employee employee = new Employee();

    BeanUtils.copyProperties(request.getEmployeeInfo(), employee);
    employeeService.updateEmployee(employee);
    ServiceStatus serviceStatus = new ServiceStatus();
    serviceStatus.setStatus("SUCCESS");
    serviceStatus.setMessage("Content Update Successfully");
    UpdateEmployeeResponse response = new UpdateEmployeeResponse();
    response.setServiceStatus(serviceStatus);
    return response;
  }


  @PayloadRoot(namespace = NameSpace_URI, localPart = "deleteEmployeeRequest")
  @ResponsePayload
  public DeleteEmployeeResponse deleteEmployee(@RequestPayload DeleteEmployeeRequest request) {
    employeeService.deleteEmployee(request.getEmployeeId());
    ServiceStatus serviceStatus = new ServiceStatus();

    serviceStatus.setStatus("SUCCESS");
    serviceStatus.setMessage("Content deleted Successfully");
    DeleteEmployeeResponse response = new DeleteEmployeeResponse();
    response.setServiceStatus(serviceStatus);
    return response;
  }

}







