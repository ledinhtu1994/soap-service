package vn.tuld.soapservice.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="OMNI_CUSTOMERS")
public class Employee implements Serializable{

	private static final long serialVersion =1l;

	@Id
	//@GeneratedValue (strategy=GenerationType.AUTO)
	@Column (name="ID")
	private long employeeId;

	@Column (name="NAME")
	private String name;

	@Column(name="department")
	private String department;

	@Column (name="phone")
	private String phone;
	@Column (name="ADDRESS")
	private String address;

	public long getEmployeeId() {
		return employeeId;
	}
	public void SetEmployeeId(long employeeId) {
		this.employeeId=employeeId;
	}

	public String getName(){
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department=department;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone=phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address=address;
	}
}