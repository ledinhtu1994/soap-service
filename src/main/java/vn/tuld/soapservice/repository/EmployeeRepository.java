package vn.tuld.soapservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.tuld.soapservice.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
  Employee findByEmployeeId(long employeeId);
}