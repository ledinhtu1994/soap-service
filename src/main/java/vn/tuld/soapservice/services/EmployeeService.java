package vn.tuld.soapservice.services;


import vn.tuld.soapservice.model.Employee;

public interface EmployeeService {
	
	void addEmployee (Employee employee);
	Employee getEmployeeById(long employeeId);
	void updateEmployee(Employee employee);
	void deleteEmployee (long employeeId);
	
}
