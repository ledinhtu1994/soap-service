package vn.tuld.soapservice.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.tuld.soapservice.model.Employee;
import vn.tuld.soapservice.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {
  @Autowired
  private EmployeeRepository employeeRepository;


  @Override
  public Employee getEmployeeById(long employeeId) {
    Employee obj = employeeRepository.findByEmployeeId(employeeId);
    return obj;
  }

  @Override
  public void addEmployee(Employee employee) {
    employeeRepository.save(employee);
  }

  @Override
  public void updateEmployee(Employee employee) {
    employeeRepository.save(employee);

  }

  @Override
  public void deleteEmployee(long employeeId) {
    employeeRepository.deleteById(employeeId);
  }
}
